package org.uaipet;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.uaipet.dto.ProcessResult;
import org.uaipet.utils.RetrofitConfig;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TokenConfirmationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_token_confirmation);

        Button confirmToken = findViewById(R.id.confirmTokenBtn);

        confirmToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText token  = findViewById(R.id.tokenTxt);

                //chamar WS para cadastrar Customer
                Call<ProcessResult> call = new RetrofitConfig().getUaiPetService().validToken("34999688006",
                        Long.valueOf(token.getText().toString()));

                call.enqueue(new Callback<ProcessResult>() {
                    @Override
                    public void onResponse(Call<ProcessResult> call, Response<ProcessResult> response) {
                        if(response.code() == 200 && response.body().getCode().equals(1000L)
                                && response.body().getApiResponse().equals(true)){
                            goToMainActivity();
                        }else{
                            Toast.makeText(getApplicationContext(),response.code() + "OPS!! Tivemos um erro, por favor tente novamente mais tarde"
                                    +response.body().toString(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProcessResult> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),"OPS!! Tivemos um erro, por favor tente novamente mais tarde", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
    }

    private void goToMainActivity(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

}


