package org.uaipet.service;

import org.uaipet.dto.Customer;
import org.uaipet.dto.Pet;
import org.uaipet.dto.ProcessResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UaiPetServices {

    @POST("addCustomer")
    Call<ProcessResult> createCustomer(@Body Customer customer);

    @GET("customer/{msisdn}")
    Call<ProcessResult> getCustomer(@Path("msisdn") String msisdn);

    @GET("/token/{msisdn}/{token}")
    Call<ProcessResult> validToken(@Path("msisdn") String msisdn, @Path("token") Long token);

    @POST("addPet")
    Call<ProcessResult> createPet(@Body Pet pet);



}
