package org.uaipet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.uaipet.dto.Customer;
import org.uaipet.dto.ProcessResult;
import org.uaipet.utils.RetrofitConfig;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {

    private static final String TAG = "RegistrationActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        Button registerButton = findViewById(R.id.register);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Customer customer = new Customer();
                EditText phone  = findViewById(R.id.phoneNumber);
                EditText email  = findViewById(R.id.email);
                EditText name  = findViewById(R.id.customerName);

                customer.setName(name.getText().toString());
                customer.setEmail(email.getText().toString());
                customer.setMsisdn(phone.getText().toString());


                //chamar WS para cadastrar Customer
                Call<ProcessResult> call = new RetrofitConfig().getUaiPetService().createCustomer(customer);

                call.enqueue(new Callback<ProcessResult>() {
                    @Override
                    public void onResponse(Call<ProcessResult> call, Response<ProcessResult> response) {
                        Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();
                        if( response.code() == 200 && response.body().getCode().equals(1000L)){
                            // direcionar para tela de token
                            System.out.println("deu certo!");
                            goToTokenActivity();
                        }else{
                            Toast.makeText(getApplicationContext(),response.code() + response.body().toString(), Toast.LENGTH_LONG).show();
                            Log.e(TAG, "onResponse: "+ response.code() + response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<ProcessResult> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),"Erro -> " + t.toString(), Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onResponse: "+ t.getMessage());
                    }
                });

            }
        });
    }

    public void goToTokenActivity(){
        Intent intent = new Intent(getApplicationContext(), TokenConfirmationActivity.class);
        startActivity(intent);
    }
}
