package org.uaipet.utils;


import org.uaipet.service.UaiPetServices;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitConfig {

    private final Retrofit retrofit;

    private static final String BASE_URL = "http://10.0.2.2:8080/";

    public RetrofitConfig() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }

    public UaiPetServices getUaiPetService() {
        return this.retrofit.create(UaiPetServices.class);
    }
}
