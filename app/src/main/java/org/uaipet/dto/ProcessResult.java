package org.uaipet.dto;

import com.google.gson.annotations.SerializedName;

public class ProcessResult {

    @SerializedName("code")
    private Long code;
    @SerializedName("message")
    private String message;
    @SerializedName("apiResponse")
    private Object apiResponse;

    public ProcessResult() {
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getApiResponse() {
        return apiResponse;
    }

    public void setApiResponse(Object apiResponse) {
        this.apiResponse = apiResponse;
    }
}
