package org.uaipet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.uaipet.dto.Pet;
import org.uaipet.dto.ProcessResult;
import org.uaipet.utils.RetrofitConfig;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPetActivity extends AppCompatActivity {

    private static final String TAG = "AddPetActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pet);

        Button registerPet = findViewById(R.id.registerPet);

        registerPet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Pet persistPet = new Pet();
                EditText petName = findViewById(R.id.petName);
                EditText petBirthDate = findViewById(R.id.petBirthDate);
                EditText petCategory = findViewById(R.id.petCategory);
                persistPet.setName(petName.getText().toString());
                persistPet.setBirthDate(new Date());
                persistPet.setCategory(petCategory.getText().toString());
                persistPet.setCustomerId(1L);


                //chamar WS para cadastrar Customer
                Call<ProcessResult> call = new RetrofitConfig().getUaiPetService().createPet(persistPet);

                call.enqueue(new Callback<ProcessResult>() {
                    @Override
                    public void onResponse(Call<ProcessResult> call, Response<ProcessResult> response) {
                        if(response.code() == 200){
                            Toast.makeText(getApplicationContext(),response.code()
                                    + response.body().toString(), Toast.LENGTH_LONG).show();
                            goToMainActivity();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProcessResult> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),t.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });



            }
        });
    }

    private void goToMainActivity(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
