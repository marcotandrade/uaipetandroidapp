package org.uaipet;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class Pager extends FragmentStatePagerAdapter {
    int tabCount;

    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                PetFragment tabPet = new PetFragment();
                return tabPet;
            case 1:
                ShoppingFragment tabShopping = new ShoppingFragment();
                return tabShopping;
            case 2:
                OngFragment tabOng = new OngFragment();
                return tabOng;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
